/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#include "ReporterFactory.h"
#include <iostream>

int main()
{
  using namespace mpv;
  reporters logs;
  ReporterParams params;
  params.setType(kReporterTypeToConsole);
  logs.add(ReporterFactory::create(params));
  params.setType(kReporterTypeToFile);
  params.setFilename("demo.log");
  logs.add(ReporterFactory::create(params));

  int i = 0;
  for (i = 0; i < 6; i++) {
    logs.fire(report(CustomReport((ResultEnum)i)));
  }
  logs.fire(report(CustomReport((ResultEnum)i)));
  logs.fire(report(0, "Success"));
  logs.fire(report(nullptr, 0));
  return 0;;
}