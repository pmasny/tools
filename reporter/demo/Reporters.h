/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "reporter.h"
#include <unordered_map>

#define DECLARE_STATIC_CREATE(classname)            \
public:                                             \
    static classname##Ptr create();

#define DEFINE_CLASS_SHARED_PTR(classname)          \
class classname;                                    \
using classname##Ptr = std::shared_ptr<classname>;

#define DEFINE_CLASS_UNIQUE_PTR(classname)          \
class classname;                                    \
using classname##Ptr = std::unique<classname>;

DEFINE_CLASS_SHARED_PTR(ReporterToConsole)
class ReporterToConsole : public mpv::reporter {
public:
    void fire(const mpv::report& whats);
    DECLARE_STATIC_CREATE(ReporterToConsole)
};

DEFINE_CLASS_SHARED_PTR(ReporterToFile)
class ReporterToFile : public mpv::reporter {
    class ReporterToFileImpl;
    std::unique_ptr<ReporterToFileImpl> m_pImpl;
public:
    ReporterToFile();
    bool init(const char *filename);
    void fire(const mpv::report& whats);
    DECLARE_STATIC_CREATE(ReporterToFile)
};

extern std::string GetMessageByError(int);
#include "ResultEnum.h"
class CustomReport : public mpv::report {
public:
    CustomReport(ResultEnum result)
        : mpv::report(GetMessageByError, (int)result) { }
};