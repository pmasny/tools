/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#include "ReporterFactory.h"

void ReporterParams::setType(ReporterType type) {
    m_type = type;
}

ReporterType ReporterParams::type() const {
    return m_type;
}

void ReporterParams::setFilename(const char *filename) {
    m_filename = filename;
}

std::string ReporterParams::filename() const {
    return m_filename;
}

const ReporterCreatorMap& ReporterFactory::reporters() {
    static const ReporterCreatorMap reporters = {
        {
            kReporterTypeToConsole,   
            [](const ReporterParams& params) -> mpv::reporter_ptr {
                return ReporterToConsole::create();  
            }
        },
        {
            kReporterTypeToFile,
            [](const ReporterParams& params) -> mpv::reporter_ptr { 
                auto reporter = ReporterToFile::create();
                reporter->init(params.filename().c_str());
                return reporter;
            }
        }
    };
    return reporters;
}

mpv::reporter_ptr ReporterFactory::create(const ReporterParams& params) {
    static mpv::reporter_ptr null_reporter;
    auto it = reporters().find(params.type());
    if (it != reporters().end())
        return it->second(params);
    return null_reporter;
}