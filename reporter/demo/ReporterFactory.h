/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "Reporters.h"

enum ReporterType : int {
    kReporterTypeToConsole = 0,
    kReporterTypeToFile
};

class ReporterParams {
    ReporterType m_type;
    std::string m_filename;
public:
    void setType(ReporterType type);
    ReporterType type() const;

    void setFilename(const char *filename);
    std::string filename() const;
};

using cbCreate = std::function<mpv::reporter_ptr(const ReporterParams&)>;
using ReporterCreatorMap = std::unordered_map<int, cbCreate>;

class ReporterFactory {
public:
    static mpv::reporter_ptr create(const ReporterParams& params);
private:
    static const ReporterCreatorMap& reporters();
};