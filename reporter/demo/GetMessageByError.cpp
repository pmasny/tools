/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <unordered_map>
#include "ResultEnum.h"

#ifdef RESULT_DEF
#undef RESULT_DEF
#endif
#define RESULT_DEF(result, desc) { (result), (desc) }

std::string GetMessageByError(int error) {
    static const std::unordered_map<int, std::string> errors = {
        #include "ResultEnumDef.h"
    };
    auto it = errors.find(error);
    if (it == errors.end())
        return "unknow error";
    return it->second;
}