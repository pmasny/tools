/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#include "Reporters.h"

#include <iostream>
#include <fstream>

class report_ext {
  const mpv::report& m_report;
public:
  report_ext(const mpv::report& rhs)
    : m_report(rhs) { }

    std::string asJson() const { 
      std::string ret = "{ \"result\": " +
                        std::to_string(m_report.result()) + 
                        ", \"desc\": " +
                        m_report.what() +
                        " } ";
      return ret;
    }
};

void ReporterToConsole::fire(const mpv::report& whats) {
    std::cout << report_ext(whats).asJson() << std::endl;
}

ReporterToConsolePtr ReporterToConsole::create() {
    return std::make_shared<ReporterToConsole>();
}

class ReporterToFile::ReporterToFileImpl {
    std::ofstream m_stream;
public:
    ReporterToFileImpl() { }
    ~ReporterToFileImpl() { close(); }
    bool init(const char *filename) {
        close();
        m_stream.open(filename);
        return m_stream.is_open();
    }
    void fire(const mpv::report& whats) {
        m_stream << report_ext(whats).asJson() << std::endl;
    }

    void close() {
        if (m_stream.is_open()) m_stream.close();
    }
};

ReporterToFile::ReporterToFile()
    : m_pImpl(new ReporterToFileImpl()) { }

bool ReporterToFile::init(const char *filename) {
    return m_pImpl->init(filename);
}

void ReporterToFile::fire(const mpv::report& whats) {
    m_pImpl->fire(whats);
}

ReporterToFilePtr ReporterToFile::create() {
    return std::make_shared<ReporterToFile>();
}