/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 11.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include <exception>
#include <functional>
#include <string>
#include <list>

namespace mpv {
using cb_get_msg_by_error = std::function<std::string(int)>;

class report : public std::runtime_error {
protected:
  int m_result;
public:
  explicit report(int error, const char *msg)
    : std::runtime_error(msg)
    , m_result(error) { }

  explicit report(cb_get_msg_by_error cb, int error) 
    : std::runtime_error( cb ? cb(error) : "undefined error handler" )
    , m_result(error) { }

  int result() const { return m_result; }
};
class reporter {
public:
  virtual ~reporter() { }
  virtual void fire(const report& whats) = 0;
};
using reporter_ptr = std::shared_ptr<reporter>;

class reporters {
  std::list<reporter_ptr> m_reporters;
public:
  void add(reporter_ptr reporter) { if (reporter) m_reporters.push_back(reporter);  }
  void fire(const report& whats)  { for (auto & r : m_reporters) r->fire(whats);    }
};
}