﻿/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 10.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////

using System;
namespace Ge
{
    public enum IntersectResult
    {
        kParallelDifferentLines,
        kSameLine,
        kIntersectionSegments,
        kIntersectionOfLinesIsNotPointOnSegmentFirst,
        kIntersectionOfLinesIsNotPointOnSegmentSecond,
        kIntersectionOfLinesIsNotPointOnSegments
    };

    public struct Line<T> where T : struct
    {
        public Point<T> First { get; set; }
        public Point<T> Second { get; set; }

        public Line(Point<T> first = default, Point<T> second = default)
        {
            First = first;
            Second = second;
        }

        public double DistanceTo(Point<T> pt) => Math.Sqrt(SqrDistanceTo(pt));

        public double SqrDistanceTo(Point<T> pt)
        {
            Vector<T> d = Second - First;
            Vector<T> YmFirst = pt - First;
            dynamic t = d.Dot(YmFirst);
            if (t <= 0)
            {
                // first is closest to Y
                return Convert.ToDouble(YmFirst.Dot(YmFirst));
            }
            dynamic sqrLengthSegment = d.Dot(d);
            if (t >= sqrLengthSegment)
            {
                // second is closest to Y
                Vector<T> YmSecond = pt - Second;
                return Convert.ToDouble(YmSecond.Dot(YmSecond));
            }
            // closest point is interior to segment
            return Convert.ToDouble(YmFirst.Dot(YmFirst) - Convert.ToDouble(t * t) / sqrLengthSegment);
        }

        public IntersectResult IntersectWith(Line<T> rhs, ref Point<T> outPt, ref double[] param, double eps = 0.001)
        {
            //P + t * dir
            Vector<T> P0 = new Vector<T>(First);
            Vector<T> dir0 = Second - First;
            Vector<T> P1 = new Vector<T>(rhs.First);
            Vector<T> dir1 = rhs.Second - rhs.First;
            Vector<T> E = P1 - P0;

            double sqrEps = eps * eps;

            dynamic kross = dir0.Cross(dir1);
            var sqrKross = kross * kross;
            dynamic sqrLenD0 = dir0.Dot(dir0);
            dynamic sqrLenD1 = dir1.Dot(dir1);
            if (sqrKross > sqrEps * sqrLenD0 * sqrLenD1)
            {
                // lines are not parallel
                double s = Convert.ToDouble(E.Cross(dir1)) / kross;
                param[0] = s;
                IntersectResult retType = IntersectResult.kIntersectionSegments;
                if (s < 0 || s > 1)
                    retType = IntersectResult.kIntersectionOfLinesIsNotPointOnSegmentFirst;
                double t = Convert.ToDouble(E.Cross(dir0)) / kross;
                param[1] = t;
                if (t < 0 || t > 1)
                    retType = (retType == IntersectResult.kIntersectionOfLinesIsNotPointOnSegmentFirst) ? IntersectResult.kIntersectionOfLinesIsNotPointOnSegments : IntersectResult.kIntersectionOfLinesIsNotPointOnSegmentSecond;
                Vector<T> result = P0 + s * dir0;
                outPt.X = result.X;
                outPt.Y = result.Y;
                return retType;
            }
            // lines are parallel
            dynamic sqrLenE = E.Dot(E);
            kross = E.Cross(dir0);
            sqrKross = kross * kross;
            if (sqrKross > sqrEps * sqrLenD0 * sqrLenE)
            {
                // lines are different
                return IntersectResult.kParallelDifferentLines;
            }
            return IntersectResult.kSameLine;
        }
    }
}
