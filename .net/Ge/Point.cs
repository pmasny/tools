﻿using System;
namespace Ge
{
    public struct Point<T> where T : struct
    {
        public T X { get; set; }
        public T Y { get; set; }

        public Point(T xx = default, T yy = default)
        {
            X = xx;
            Y = yy;
        }

        public T SqrDistanceTo(Point<T> pt)
        {
            dynamic dx = X, dptx = pt.X;
            dynamic dy = Y, dpty = pt.Y;
            
            dynamic diffx = dx - dptx;
            dynamic diffy = dy - dpty;

            return diffx * diffx + diffy * diffy;
        }

        double DistanceTo(Point<T> pt)
        {
            return Math.Sqrt(Convert.ToDouble(SqrDistanceTo(pt)));
        }

        public static Vector<T> operator -(Point<T> f, Point<T> s)
            => new Vector<T>((dynamic)f.X - (dynamic)s.X, (dynamic)f.Y - (dynamic)s.Y);

        public static Vector<T> operator +(Point<T> f, Point<T> s)
            => new Vector<T>((dynamic)f.X + (dynamic)s.X, (dynamic)f.Y + (dynamic)s.Y);
    } 
}
