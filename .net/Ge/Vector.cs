﻿/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 10.2019,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////

namespace Ge
{
    public struct Vector<T> where T : struct
    {
        public T X { get; set; }
        public T Y { get; set; }

        public Vector(T xx = default, T yy = default)
        {
            X = xx;
            Y = yy;
        }

        public Vector(Point<T> pt)
        {
            X = pt.X;
            Y = pt.Y;
        }

        public T Dot(Vector<T> rhs)
            => ((dynamic)X * (dynamic)rhs.X + (dynamic)Y * (dynamic)rhs.Y);

        public T Cross(Vector<T> rhs)
            => ((dynamic)X * (dynamic)rhs.Y - (dynamic)Y * (dynamic)rhs.X);

        public static Vector<T> operator +(Vector<T> f, Vector<T> s)
            => new Vector<T>((dynamic)f.X + (dynamic)s.X, (dynamic)f.Y + (dynamic)s.Y);

        public static Vector<T> operator -(Vector<T> f, Vector<T> s)
            => new Vector<T>((dynamic)f.X - (dynamic)s.X, (dynamic)f.Y - (dynamic)s.Y);

        public static Vector<T> operator *(double value, Vector<T> rhs)
            => new Vector<T>(value * (dynamic)rhs.X, value * (dynamic)rhs.Y);
    }
}
