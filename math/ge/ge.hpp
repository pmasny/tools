/////////////////////////////////////////////////////////////////////////////// 
// CopyLeft 2018,
// Pavel Masny authors (mailto:pmasny127@icloud.com)
///////////////////////////////////////////////////////////////////////////////
#pragma once

namespace mpv {
#include <math.h>

class Point2d
{
public:
    double x;
    double y;

public:
    explicit Point2d(double xx = double(0), double yy = double(0)) { x = xx; y = yy; }
    inline double distanceTo(const Point2d& pt) const { return sqrt(sqrDistanceTo(pt)); }
    inline double sqrDistanceTo(const Point2d& pt) const { double dx = x - pt.x; double dy = y - pt.y; return dx * dx + dy * dy; }
};

class Vector2d
{
public:
    double x;
    double y;
public:
    explicit Vector2d(double xx = 0.f, double yy = 0.f) { x = xx; y = yy; }
    explicit Vector2d(const Point2d& rhs) { x = rhs.x; y = rhs.y; }
    inline double dot(const Vector2d& rhs) const {
        return x * rhs.x + y * rhs.y;
    }

    inline double cross(const Vector2d& rhs) const {
        return x * rhs.y - rhs.x * y;
    }

    inline Vector2d& operator-(const Vector2d& rhs) {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    inline Vector2d& operator+(const Vector2d& rhs) {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    inline Vector2d& operator=(const Vector2d& rhs) {
        x = rhs.x;
        y = rhs.y;
        return *this;
    }
};

inline Vector2d operator-(const Point2d lhs, const Point2d& rhs) {
    return Vector2d(lhs.x - rhs.x, lhs.y - rhs.y);
}


inline Vector2d operator*(double value, const Vector2d& rhs) {
    return Vector2d(double(value * rhs.x), double(value * rhs.y));
}


inline Vector2d operator*(float value, const Vector2d& rhs) {
    return Vector2d(double(value * rhs.x), double(value * rhs.y));
}


inline Point2d operator + (const Point2d lhs, const Vector2d& rhs) {
    return Point2d(lhs.x + rhs.x, lhs.y + rhs.y);
}


inline Point2d operator - (const Point2d lhs, const Vector2d& rhs) {
    return Point2d(lhs.x - rhs.x, lhs.y - rhs.y);
}

enum IntersectType {
    kParallelDifferentLines,
    kSameLine,
    kIntersectionSegments,
    kIntersectionOfLinesIsNotPointOnSegmentFirst,
    kIntersectionOfLinesIsNotPointOnSegmentSecond,
    kIntersectionOfLinesIsNotPointOnSegments
};


class Line2d
{
public:
    Point2d first;
    Point2d second;

    Line2d() { }
    Line2d(const Point2d& f, const Point2d& s) : first(f), second(s) { }

    inline double distanceTo(const Point2d& pt) const {
        return sqrt(sqrDistanceTo(pt));
    }

    inline double sqrDistanceTo(const Point2d& pt) const {
       Vector2d d = second - first;
       Vector2d YmFirst = pt - first;
        double t = d.dot(YmFirst);
        if (t <= 0) {
            // first is closest to Y
            return YmFirst.dot(YmFirst);
        }
        double sqrLengthSegment = d.dot(d);
        if (t >= sqrLengthSegment) {
            // second is closest to Y
           Vector2d YmSecond = pt - second;
           return YmSecond.dot(YmSecond);
        }
        // closest point is interior to segment
        return double(YmFirst.dot(YmFirst) - double(t * t) / sqrLengthSegment);
    }

    inline IntersectType intersectWith(const Line2d& rhs, Point2d& out, double params[2], double eps = 0.001) const {
        // P + t * dir
       Vector2d P0(first);
       Vector2d dir0(second - first);
       Vector2d P1(rhs.first);
       Vector2d dir1(rhs.second - rhs.first);
       Vector2d E = P1 - P0;

        double sqrEps = eps * eps;
        
        double kross = dir0.cross(dir1);
        double sqrKross = kross * kross;
        double sqrLenD0 = dir0.dot(dir0);
        double sqrLenD1 = dir1.dot(dir1);
        if (sqrKross > sqrEps * sqrLenD0 * sqrLenD1) {
            // lines are not parallel
            double s = double(E.cross(dir1)) / kross;
            params[0] = s;
            IntersectType retType = kIntersectionSegments;
            if (s < 0.f || s > 1.f)
                retType = kIntersectionOfLinesIsNotPointOnSegmentFirst;
            double t = double(E.cross(dir0)) / kross;
            params[1] = t;
            if (t < 0.f || t > 1.f)
                retType = (retType == kIntersectionOfLinesIsNotPointOnSegmentFirst) ? kIntersectionOfLinesIsNotPointOnSegments : kIntersectionOfLinesIsNotPointOnSegmentSecond;
            Vector2d result = P0 + s * dir0;
            out.x = result.x;
            out.y = result.y;
            return retType;
        }
        // lines are parallel
        double sqrLenE = E.dot(E);
        kross = E.cross(dir0);
        sqrKross = kross * kross;
        if (sqrKross > sqrEps * sqrLenD0 * sqrLenE) {
            // lines are different
            return kParallelDifferentLines;
        }
        return kSameLine;
    }

    inline Point2d getPointAt(double param) {
        Vector2d dir(second - first);
        Vector2d offset(double(dir.x * param), double(dir.y * param));
        return first + offset;
    }
};
}


